from distutils.core import setup
import os
import sysconfig
import sys


setup(
		name='BlueBird',
		version='0.0.dev0',
		author='Giuliano Iorio',
		author_email='',
		url='',
		packages=['BlueBird'],
        install_requires=['requests', 'json', 'numpy','pandas'],
		zip_safe=False
)
