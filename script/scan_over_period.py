from BlueBird import Scan_flight
import time
import yagmail
import pandas as pd

# connect to smtp server.
#subject = 'Hello from richard'
# email content with attached file path.
#contents = ['Hello tom this is richard speaking']
# send the email
#yag_smtp_connection.send('giuliano.iorio89@gmail.com', subject, contents)


from_='CIA'
to_='STN'
date='2019-08-25'
days=8
sleep= 2 #in h
max_time = 5 #days
time_summary = 2 #in h

def info_min(min_):
	h='%s  %i  %s --> %s %i %.2f'%(pd.Timestamp(min_.date).strftime("%d-%m-%Y"),min_.code, min_.departure_time, min_.arrival_time, min_.n_left, min_.price)
	return h

yag_smtp_connection = yagmail.SMTP('bluebird.develop@gmail.com', oauth2_file='~/.oauth2_creds.json')

s=Scan_flight(from_,to_,date)
print('Hello! Let\'s start to check some flights',flush=True)
s.scan(days)
min_fare,min_object=s.min_fare()
initial_info = s.__str__()
print('Uff, better to save the tables')
s.save_and_reset(name_scan='last_scan', name_historic='historic_scans', directory='.')
min_global=min_fare
min_object_global=min_object

print('Sending a mail to inform')
mail_content='BlueBird just started a scan with parameters:\n\n FROM: %s\n TO: %s\n START DATE: %s\n DAYS: %i\n'%(from_,to_,date,days)
mail_content+='\nINITIAL CHEAPEST FLIGHT:\n%s\n\n\n\n\n'%info_min(min_object_global)
mail_content_new=initial_info
subject = 'BlueBird scan %s-%s'%(from_,to_)
yag_smtp_connection.send('giuliano.iorio89@gmail.com', subject, mail_content+mail_content_new)


time_limit=pd.Timestamp.now()+ pd.Timedelta(max_time,unit='d')
time_counter=0


while (pd.Timestamp.now()<time_limit):
	mail_content_new=""
	tstart=time.time()
	print('Going to sleep now!G\'night..',flush=True)
	time.sleep(sleep*3600)
	print('Charge old scans')
	s.load_historic_tab('historic_scans.csv')
	print('Ehy, I am awake, let\'s check some flights', flush=True)
	s.scan(days)
	min_fare,min_object=s.min_fare()

	if min_fare<min_global:
		print('Wow I found a new cheaper flight')
		min_global=min_fare
		min_object_gloal=min_object
		print('Sending a mail to inform')
		subject = 'BlueBird scan %s-%s New Cheapest flight'%(from_,to_)
		mail_content_new+='\nNEW CHEAPEST FLIGHT:\n%s'%info_min(min_object)
		yag_smtp_connection.send('giuliano.iorio89@gmail.com', subject, mail_content+mail_content_new)
	elif min_fare>min_global:
		print('Oh no the cheaper flight is rising')
		min_global=min_fare
		min_object_gloal=min_object
		print('Sending a mail to inform')
		subject = 'BlueBird scan %s-%s New Cheapest flight'%(from_,to_)
		mail_content_new+='\nNEW CHEAPEST FLIGHT:\n%s'%info_min(min_object)
		yag_smtp_connection.send('giuliano.iorio89@gmail.com', subject, mail_content+mail_content_new)

	tend=time.time()
	time_counter+=tend-tstart

	mail_content_new=""
	if time_counter/3600 > time_summary:
		print('Sending a summary')
		s_summary = s.__str__()
		subject = 'BlueBird scan %s-%s Summary'%(from_,to_)
		mail_content_new+='\nCHEAPEST FLIGHT:\n%s \n\n %s'%(info_min(min_object_global),s_summary)
		yag_smtp_connection.send('giuliano.iorio89@gmail.com', subject, mail_content+mail_content_new)
		time_counter=0

	print('Uff, better to save the tables')
	s.save_and_reset(name_scan='last_scan_%s_%s'%(from_,to_), name_historic='historic_scans_%s_%s'%(from_,to_), directory='.')
