import requests
import json
from datetime import datetime
import pandas as pd
import numpy as np

class Scan_flight:

    def __init__(self,depart, arrive, start_day, market='en-gb', query_link='https://desktopapps.ryanair.com/en-gb/availability'):
        """
        Start date should be a Timestamp of datetime object or a string with format 'YYYY-mm-dd'
        """
        self.depart = depart
        self.arrive = arrive
        self.start_day = start_day
        self.market = market
        self.query_link = query_link
        self.idx_query = 0

        if isinstance(start_day,datetime) or isinstance(start_day,pd.Timestamp):
            self.start_day = start_day.strftime("%Y-%m-%d")
        else:
            self.start_day = start_day

        #table where to store the last scan
        self._last_scan_tab     = None
        #table where to store  the history of all the scans
        self._historic_scan_tab = None
        self._initialise_tab() #Put the right columns in the tabs

    @property
    def query_url(self):
        return  self._run_query(0).url

    @property
    def scan_tab(self):

        return self._last_scan_tab

    @property
    def historic_scan_tab(self):

        return self._historic_scan_tab

    def scan(self, days=1, save_in_table=True):
        if days<1: raise ValueError('Number of days to scan needs to be larger than 1')

        d=self._scan(days)
        if save_in_table:  self._put_in_table(d)

        return self.__str__()

    def load_historic_tab_from_dataframe(self,dataframe,overwrite=False):


        columns_input   =  np.sort(list(dataframe.columns.values))
        columns_current =  np.sort(list(self._historic_scan_tab.columns.values))

        #CHECK
        if self._historic_scan_tab.empty==False and overwrite==False:
            raise ValueError('Historic scan table is not empty and overwrite is set to False')
        elif len(columns_input)!=len(columns_current):
            raise ValueError('The columns of the loaded array are not in right format: they are %s, but should be %s'%(str(columns_input),str(columns_current)))
        elif np.any(columns_input!=columns_current):
            raise ValueError('The columns of the loaded array are not in right format: they are %s, but should be %s'%(str(columns_input),str(columns_current)))
        else:
            self._historic_scan_tab=dataframe

        return 0

    def load_historic_tab_from_file(self,file,overwrite=False):

        dataframe=pd.read_csv(file)

        self.load_historic_tab_from_dataframe(dataframe,overwrite=overwrite)

        return 0

    def load_historic_tab(self,load,overwrite=False):

        if isinstance(load, pd.DataFrame): self.load_historic_tab_from_dataframe(load,overwrite=overwrite)
        else: self.load_historic_tab_from_file(load,overwrite=overwrite)

        return 0

    def reset_tab(self):
        self._initialise_tab()
        return 0

    def min_fare(self):
        if self._last_scan_tab.empty: raise ValueError('Scan table is empty, run a scan before')
        min_v=self.scan_tab.loc[self.scan_tab["price"].idxmin()]
        return min_v.price,min_v

    def _initialise_tab(self):
        #table where to store the last scan
        self._last_scan_tab     = pd.DataFrame(columns=['idx_flight','date', 'flight_weekday', 'origin','destination','code','departure_time','arrival_time','n_left','price','days_from_query','idx_query', 'query_time','query_weekday','query_date'])
        #table where to store  the history of all the scans
        self._historic_scan_tab = pd.DataFrame(columns=['idx_flight','date', 'flight_weekday', 'origin','destination','code','departure_time','arrival_time','n_left','price','days_from_query','idx_query', 'query_time','query_weekday','query_date'])

        return 0



    def save_scan_table(self,name=None, directory='.'):

        if name is None: name = 'scan_'+datetime.now().strftime("%d_%m_%Y_%H_%M")

        save_name=directory+'/'+name+'.csv'
        self._save_to_csv(self._last_scan_tab, save_name)

        return save_name

    def save_historic_table(self,name=None, directory='.'):

        if name is None: name = 'historic_scan_'+datetime.now().strftime("%d_%m_%Y_%H_%M")

        save_name=directory+'/'+name+'.csv'
        self._save_to_csv(self._historic_scan_tab , save_name)

        return save_name

    def save_tables(self,name_scan=None, name_historic=None, directory='.'):

        save_name_scan = self.save_scan_table(name_scan, directory)
        save_name_historic = self.save_historic_table(name_historic, directory)

        return (save_name_scan , save_name_historic)

    def save_and_reset(self,name_scan=None, name_historic=None, directory='.'):

        save_names = self.save_tables(name_scan, name_historic, directory)
        self.reset_tab()

        return save_names

    def _save_to_csv(self,dataframe,savename):

        dataframe.to_csv(savename,sep=',',header=True, index=False)

        return 0

    def _scan(self,days):
        scan_days=days-1
        days_limit=6
        if scan_days<=days_limit:
            d=self._run_query(scan_days).json()
        else:
            N_cycle = scan_days//days_limit if scan_days%days_limit==0 else  scan_days//days_limit+1
            current_day = pd.Timestamp(self.start_day)
            days_left=scan_days
            list_of_dates=[]
            for i in range(N_cycle):
                scan_days_t = 6 if days_left>7 else (days_left-1)
                day_t=current_day.strftime("%Y-%m-%d")
                d=self._run_query(scan_days_t, start_day=day_t).json()
                list_of_dates=list_of_dates+d['trips'][0]['dates']
                #Update
                days_left-=scan_days_t
                time_shift = 7
                current_day += pd.Timedelta(time_shift,unit='d')
            d['trips'][0]['dates']=list_of_dates
        self.idx_query+=1
        return d

    def _run_query(self,days,start_day=None):
        if start_day is None: start_day=self.start_day
        r = requests.get(self.query_link,params={'ADT':'1', 'CHD':'0', 'TEEN':'0',  'Origin':self.depart, 'Destination':self.arrive, 'market':self.market, 'DateOut':start_day, 'RoundTrip':'false', 'FlexDaysOut':str(days), 'ToUs':'AGREED'})
        return r

    def _put_in_table(self,json_result):
        """
        Take the result of a query and put it in a pandas dataframe.
        Optionally, you can append this data to another historic dataframe that shoud have the same columns:
        'date', 'origin','destination','code','departure_time','arrival_time','n_left','price', 'query_time','date_from_query'
        """

        d = json_result
        trips = d['trips'][0]
        Ndates=len(trips['dates'])

        for date in trips['dates']:
            origin=trips['origin']
            destination=trips['destination']
            flights=date['flights']
            date_=date['dateOut'].split('T')[0]
            date_ = pd.Timestamp.strptime(date['dateOut'], "%Y-%m-%dT%H:%M:%S.%f")
            flight_weekday=date_.strftime('%a')
            query_date=pd.Timestamp(datetime.now())
            query_date_float = query_date.hour + query_date.minute/60 + query_date.minute/3600
            query_weekday=query_date.strftime('%a')
            date_diff = date_.to_julian_date() - query_date.to_julian_date()
            for flight in flights:
                if flight['faresLeft']>0:
                    time_d=pd.Timestamp.strptime(flight['time'][0], "%Y-%m-%dT%H:%M:%S.%f")
                    time_a=pd.Timestamp.strptime(flight['time'][1], "%Y-%m-%dT%H:%M:%S.%f")
                    code=int(flight['flightNumber'].split(' ')[1])
                    idx_flight=int(date_.strftime("%Y%m%d")+str(code))
                    for fare in flight['regularFare']['fares']:
                        self._last_scan_tab=self._last_scan_tab.append({'idx_flight':idx_flight,'date':date_, 'flight_weekday':flight_weekday, 'origin':origin,'destination':destination, 'code':code, 'departure_time':time_d.strftime("%H:%M"),'arrival_time':time_a.strftime("%H:%M"),'n_left':flight['faresLeft'],'price':fare['amount'], 'days_from_query':float(date_diff),'idx_query':self.idx_query,'query_time':query_date_float, 'query_weekday':query_weekday,'query_date':query_date},ignore_index=True)

        self._historic_scan_tab=pd.concat([self._historic_scan_tab,self._last_scan_tab])

        return 0#tab, updated_historic_table

    def __str__(self):

        h=''
        if self._last_scan_tab.empty:
            h+='Data Scan table is empty'
        else:
            tab=self._last_scan_tab
            minimum_price_per_day=pd.DataFrame(tab.loc[tab.groupby("date")["price"].idxmin()]).reset_index()
            min_prices=minimum_price_per_day['price'].values
            min_prices_departure_time=minimum_price_per_day['departure_time'].values
            dates_in_table=tab.date.unique()
            for k,date in enumerate(dates_in_table):
                tab_t=tab[tab['date']==date].sort_values(by=['price'])
                h+='%s; Cheapest= %.2f at %s\n'%(pd.Timestamp(date).strftime("%d-%m-%Y"), min_prices[k],min_prices_departure_time[k])
                #print(pd.Timestamp(date).strftime("%d-%m-%Y"),'; Cheapest=',min_prices[k], 'at',min_prices_departure_time[k])
                for index,row in tab_t.iterrows():
                    h+='    %i %s --> %s %i %.2f\n'%(row.code, row.departure_time,row.arrival_time,row.n_left, row.price)
                    #print('    ',row.code, row.departure_time,'-->',row.arrival_time,row.n_left, row.price )

        return h


if __name__=='__main__':

    from_='STN'
    to_='CIA'
    market='en-gb'
    day='2019-08-01'
    day_plus=10

    s=Scan_flight(from_, to_, day)
    #print(s.query_url)
    print(s.scan(10))
    print(s.save_and_reset('scan','historic'))
    print(s)
    s.load_historic_tab('historic.csv')
    print(s.historic_scan_tab)
